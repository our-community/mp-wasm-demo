// index.js
require('../../assets/wasm_exec_mp.js')

Page({
  data: {
    total: 0,
    plainText: undefined,
    signature: "-"
  },
  // 事件处理函数
  addTotal() {
    this.setData({
      total: global.addTotal()
    })
  },
  plainTextInput(e) {
    this.setData({
      plainText: e.detail.value
    })
  },
  signature() {
    const that = this
    this.setData({
      signature: global.signature(that.data.plainText)
    })
  },
  async onLoad() {
    await this.initGo()
  },
  async initGo() {
    global.console = console
		const go = new global.Go();
		try {
			const result = await WXWebAssembly.instantiate('assets/main.wasm.br', go.importObject)
			await go.run(result.instance);
		} catch (err) {
			console.error('initGo', err)
		}
	}
})
